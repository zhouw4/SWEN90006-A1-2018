package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

/* 
  //Any method annotation with "@Test" is executed as a test.
  @Test public void aTest()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    final int expected = 2;
    final int actual = 1 + 1;
    assertEquals(expected, actual);
  }

  @Test public void anotherTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("a");
    list.add("b");

    //the assertTrue method is used to check whether something holds.
    assertTrue(list.contains("a"));
  }

  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }
  
  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
  @Test public void aFailedTest()
  {
    //include a message for better feedback
    final int expected = 2;
    final int actual = 1 + 2;
    assertEquals("Some failure message", expected, actual);
  }
*/

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
  
  @Test(expected = InvalidInstructionException.class) 
  public void EC1_Test(){
	  Machine m = new Machine();
	  List<String> EC1 = new ArrayList<String>();
	  EC1.add("WRONG INPUT");
	  EC1.add("RET R0");
	  m.execute(EC1);
  }
  
  @Test
  public void EC3_Test(){
	  Machine m = new Machine();
	  List<String> EC3 = new ArrayList<String>();
	  EC3.add(";; This is comment! ");
	  EC3.add("RET R0");
	  final int actual = 0;
	  assertEquals(m.execute(EC3), actual);
  }
  
  @Test(expected = InvalidInstructionException.class) 
  public void EC12_Test(){
	  Machine m = new Machine();
	  List<String> EC12 = new ArrayList<String>();
	  EC12.add("ADD R32 R0 R1; Comment ");
	  EC12.add("RET R0");
	  m.execute(EC12);
  }
  
  @Test
  public void EC13_Test(){
	  Machine m = new Machine();
	  List<String> EC13 = new ArrayList<String>();
	  EC13.add("MOV R1 1");
	  EC13.add("ADD R0 R1 R2");
	  EC13.add("RET R0");
	  final int actual = 1;
	  assertEquals(m.execute(EC13), actual);
  }
  
  @Test
  public void EC14_Test(){
	  Machine m = new Machine();
	  List<String> EC14 = new ArrayList<String>();
	  EC14.add("MOV R0 1");
	  EC14.add("DIV R0 R1 R2");
	  EC14.add("RET R0");
	  final int actual = 1;
	  assertEquals(m.execute(EC14), actual);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void EC31_Test(){
	  Machine m = new Machine();
	  List<String> EC31 = new ArrayList<String>();
	  EC31.add("MOV R0 1");
	  EC31.add("MOV R1 1");
	  EC31.add("DIV R32 R1 R0");
	  EC31.add("RET R1");
	  m.execute(EC31);
  }
  
  @Test
  public void EC32_Test(){
	  Machine m = new Machine();
	  List<String> EC32 = new ArrayList<String>();
	  EC32.add("MOV R0 2");
	  EC32.add("MOV R1 4");
	  EC32.add("DIV R2 R1 R0");
	  EC32.add("RET R2");
	  final int actual = 2;
	  assertEquals(m.execute(EC32), actual);
  }
  
  @Test
  public void EC16_Test(){
	  Machine m = new Machine();
	  List<String> EC16 = new ArrayList<String>();
	  EC16.add("LDR R0 R1 -1");
	  EC16.add("RET R0");
	  final int actual = 0;
	  assertEquals(m.execute(EC16), actual);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void EC18_Test(){
	  Machine m = new Machine();
	  List<String> EC18 = new ArrayList<String>();
	  EC18.add("LDR R0 R1 65536");
	  EC18.add("RET R0");
	  m.execute(EC18);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void EC33_Test(){
	  Machine m = new Machine();
	  List<String> EC33 = new ArrayList<String>();
	  EC33.add("MOV R0 10");
	  EC33.add("STR R1 0 R0");
	  EC33.add("LDR R2 R32 0");
	  EC33.add("RET R2");
	  m.execute(EC33);
  }
  
  @Test
  public void EC34_Test(){
	  Machine m = new Machine();
	  List<String> EC34 = new ArrayList<String>();
	  EC34.add("MOV R0 10");
	  EC34.add("STR R1 0 R0");
	  EC34.add("LDR R2 R3 0");
	  EC34.add("RET R2");
	  final int actual = 10;
	  assertEquals(m.execute(EC34), actual);
  }
  
  @Test
  public void EC20_Test(){
	  Machine m = new Machine();
	  List<String> EC20 = new ArrayList<String>();
	  EC20.add("MOV R0 1");
	  EC20.add("JZ R0 1");
	  EC20.add("MOV R2 2");
	  EC20.add("RET R2");
	  final int actual = 2;
	  assertEquals(m.execute(EC20), actual);
  }
  
  @Test(expected = NoReturnValueException.class)
  public void EC44_Test(){
	  Machine m = new Machine();
	  List<String> EC44 = new ArrayList<String>();
	  EC44.add("JZ R0 -10");
	  EC44.add("RET R0");
	  m.execute(EC44);
  }
  
  @Test
  public void EC45_Test(){
	  Machine m = new Machine();
	  List<String> EC45 = new ArrayList<String>();
	  EC45.add("JZ R0 2");
	  EC45.add("MOV R1 2");
	  EC45.add("MOV R2 2");
	  EC45.add("RET R1");
	  final int actual = 0;
	  assertEquals(m.execute(EC45), actual);
  }
  
  @Test(expected = NoReturnValueException.class)
  public void EC46_Test(){
	  Machine m = new Machine();
	  List<String> EC46 = new ArrayList<String>();
	  EC46.add("JZ R0 10");
	  EC46.add("RET R0");
	  m.execute(EC46);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void EC21_Test(){
	  Machine m = new Machine();
	  List<String> EC21 = new ArrayList<String>();
	  EC21.add("RET R32");
	  EC21.add("RET R0");
	  m.execute(EC21);
  }
  
  @Test
  public void EC22_Test(){
	  Machine m = new Machine();
	  List<String> EC22 = new ArrayList<String>();
	  EC22.add("RET R2");
	  final int actual = 0;
	  assertEquals(m.execute(EC22), actual);
  }
  
  @Test(expected = NoReturnValueException.class)
  public void EC37_Test(){
	  Machine m = new Machine();
	  List<String> EC37 = new ArrayList<String>();
	  EC37.add("JMP -10");
	  EC37.add("RET R0");
	  m.execute(EC37);
  }
  
  @Test
  public void EC38_Test(){
	  Machine m = new Machine();
	  List<String> EC38 = new ArrayList<String>();
	  EC38.add("JMP 2");
	  EC38.add("MOV R1 2");
	  EC38.add("MOV R2 2");
	  EC38.add("RET R1");
	  final int actual = 0;
	  assertEquals(m.execute(EC38), actual);
  }
  
  @Test(expected = NoReturnValueException.class)
  public void EC39_Test(){
	  Machine m = new Machine();
	  List<String> EC39 = new ArrayList<String>();
	  EC39.add("JMP 10");
	  EC39.add("RET R0");
	  m.execute(EC39);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void EC25_Test(){
	  Machine m = new Machine();
	  List<String> EC25 = new ArrayList<String>();
	  EC25.add("MOV R0 -65537");
	  EC25.add("RET R0");
	  m.execute(EC25);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void EC27_Test(){
	  Machine m = new Machine();
	  List<String> EC27 = new ArrayList<String>();
	  EC27.add("MOV R0 65536");
	  EC27.add("RET R0");
	  m.execute(EC27);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void EC40_Test(){
	  Machine m = new Machine();
	  List<String> EC40 = new ArrayList<String>();
	  EC40.add("MOV R32 1");
	  EC40.add("RET R0");
	  m.execute(EC40);
  }
  
  @Test
  public void EC41_Test(){
	  Machine m = new Machine();
	  List<String> EC41 = new ArrayList<String>();
	  EC41.add("MOV R0 1");
	  EC41.add("RET R0");
	  final int actual = 1;
	  assertEquals(m.execute(EC41), actual);
  }
  
  @Test
  public void EC28_Test(){
	  Machine m = new Machine();
	  List<String> EC28 = new ArrayList<String>();
	  EC28.add("MOV R1 1");
	  EC28.add("STR R0 -1 R1");
	  EC28.add("LDR R2 R3 0");
	  EC28.add("RET R2");
	  final int actual = 0;
	  assertEquals(m.execute(EC28), actual);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void EC30_Test(){
	  Machine m = new Machine();
	  List<String> EC30 = new ArrayList<String>();
	  EC30.add("MOV R1 1");
	  EC30.add("STR R0 65536 R1");
	  EC30.add("LDR R2 R3 0");
	  EC30.add("RET R2");
	  m.execute(EC30);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void EC42_Test(){
	  Machine m = new Machine();
	  List<String> EC42 = new ArrayList<String>();
	  EC42.add("MOV R1 1");
	  EC42.add("STR R32 10 R1");
	  EC42.add("LDR R2 R3 0");
	  EC42.add("RET R2");
	  m.execute(EC42);
  }
  
  @Test
  public void EC43_Test(){
	  Machine m = new Machine();
	  List<String> EC43 = new ArrayList<String>();
	  EC43.add("MOV R1 10");
	  EC43.add("STR R0 0 R1");
	  EC43.add("LDR R2 R3 0");
	  EC43.add("RET R2");
	  final int actual = 10;
	  assertEquals(m.execute(EC43), actual);
  }
}

package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

/*
  //Any method annotation with "@Test" is executed as a test.
  @Test public void aTest()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    final int expected = 2;
    final int actual = 1 + 1;
    assertEquals(expected, actual);
  }

  @Test public void anotherTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("a");
    list.add("b");

    //the assertTrue method is used to check whether something holds.
    assertTrue(list.contains("a"));
  }

  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }
  
  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
  @Test public void aFailedTest()
  {
    //include a message for better feedback
    final int expected = 2;
    final int actual = 1 + 2;
    assertEquals("Some failure message", expected, actual);
  }
*/
  
  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void num1_Test(){
	  Machine m = new Machine();
	  List<String> num1 = new ArrayList<String>();
	  num1.add("ADD R32 R0 R1");
	  num1.add("RET R0");

	  m.execute(num1);
  }
  
  @Test
  public void num2_Test(){
	  Machine m = new Machine();
	  List<String> num2 = new ArrayList<String>();
	  num2.add("ADD R0 R1 R2");
	  num2.add("RET R0");
	  final int actual = 0;
	  assertEquals(m.execute(num2), actual);
  }
  
  @Test
  public void num3_Test(){
	  Machine m = new Machine();
	  List<String> num3 = new ArrayList<String>();
	  num3.add("MOV R0 1");
	  num3.add("DIV R0 R1 R2");
	  num3.add("RET R0");
	  final int actual = 1;
	  assertEquals(m.execute(num3), actual);
  }
  
  @Test
  public void num4_Test(){
	  Machine m = new Machine();
	  List<String> num4 = new ArrayList<String>();
	  num4.add("MOV R0 3");
	  num4.add("MOV R1 -1");
	  num4.add("DIV R2 R0 R1");
	  num4.add("RET R2");
	  final int actual = -3;
	  assertEquals(m.execute(num4), actual);
  }
  
  @Test
  public void num5_Test(){
	  Machine m = new Machine();
	  List<String> num5 = new ArrayList<String>();
	  num5.add("MOV R0 3");
	  num5.add("MOV R1 1");
	  num5.add("DIV R2 R0 R1");
	  num5.add("RET R2");
	  final int actual = 3;
	  assertEquals(m.execute(num5), actual);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void num6_Test(){
	  Machine m = new Machine();
	  List<String> num6 = new ArrayList<String>();
	  num6.add("MOV R1 1");
	  num6.add("DIV R32 R0 R1");
	  num6.add("RET R0");

	  m.execute(num6);
  }
  
  @Test
  public void num7_Test(){
	  Machine m = new Machine();
	  List<String> num7 = new ArrayList<String>();
	  num7.add("MOV R2 2");
	  num7.add("MOV R3 2");
	  num7.add("DIV R0 R3 R2");
	  num7.add("RET R0");
	  final int actual = 1;
	  assertEquals(m.execute(num7), actual);
  }
  
  @Test
  public void num8_Test(){
	  Machine m = new Machine();
	  List<String> num8 = new ArrayList<String>();
	  num8.add("LDR R0 R1 0");
	  num8.add("RET R0");
	  final int actual = 0;
	  assertEquals(m.execute(num8), actual);
  }
  
  @Test
  public void num9_Test(){
	  Machine m = new Machine();
	  List<String> num9 = new ArrayList<String>();
	  num9.add("LDR R0 R1 65535");
	  num9.add("RET R0");
	  final int actual = 0;
	  assertEquals(m.execute(num9), actual);
  }
  
  @Test
  public void num10_Test(){
	  Machine m = new Machine();
	  List<String> num10 = new ArrayList<String>();
	  num10.add("MOV R0 1");
	  num10.add("LDR R0 R1 -1");
	  num10.add("RET R0");
	  final int actual = 1;
	  assertEquals(m.execute(num10), actual);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void num11_Test(){
	  Machine m = new Machine();
	  List<String> num11 = new ArrayList<String>();
	  num11.add("LDR R0 R1 65536");
	  num11.add("RET R0");
	  m.execute(num11);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void num12_Test(){
	  Machine m = new Machine();
	  List<String> num12 = new ArrayList<String>();
	  num12.add("LDR R2 R32 0");
	  num12.add("RET R2");
	  m.execute(num12);
  }
  
  @Test
  public void num13_Test(){
	  Machine m = new Machine();
	  List<String> num13 = new ArrayList<String>();
	  num13.add("LDR R0 R2 0");
	  num13.add("RET R0");
	  final int actual = 0;
	  assertEquals(m.execute(num13), actual);
  }
  
  @Test
  public void num14_Test(){
	  Machine m = new Machine();
	  List<String> num14 = new ArrayList<String>();
	  num14.add("MOV R0 1");
	  num14.add("JZ R0 1");
	  num14.add("MOV R2 2");
	  num14.add("RET R2");
	  final int actual = 2;
	  assertEquals(m.execute(num14), actual);
  }
  
  @Test(expected = NoReturnValueException.class)
  public void num16_Test(){
	  Machine m = new Machine();
	  List<String> num16 = new ArrayList<String>();
	  num16.add("JZ R0 -1");
	  num16.add("RET R0");
	  m.execute(num16);
  }
  
  @Test(expected = NoReturnValueException.class)
  public void num17_Test(){
	  Machine m = new Machine();
	  List<String> num17 = new ArrayList<String>();
	  num17.add("JZ R0 2");
	  num17.add("RET R0");
	  m.execute(num17);
  }
  
  @Test
  public void num18_Test(){
	  Machine m = new Machine();
	  List<String> num18 = new ArrayList<String>();
	  num18.add("JZ R0 3");
	  num18.add("MOV R1 2");
	  num18.add("MOV R2 2");
	  num18.add("RET R1");
	  final int actual = 0;
	  assertEquals(m.execute(num18), actual);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void num19_Test(){
	  Machine m = new Machine();
	  List<String> num19 = new ArrayList<String>();
	  num19.add("RET R32");
	  m.execute(num19);
  }
  
  @Test
  public void num20_Test(){
	  Machine m = new Machine();
	  List<String> num20 = new ArrayList<String>();
	  num20.add("RET R31");
	  final int actual = 0;
	  assertEquals(m.execute(num20), actual);
  }
  
  @Test(expected = NoReturnValueException.class)
  public void num22_Test(){
	  Machine m = new Machine();
	  List<String> num22 = new ArrayList<String>();
	  num22.add("JMP -1");
	  num22.add("RET R0");
	  m.execute(num22);
  }
  
  @Test(expected = NoReturnValueException.class)
  public void num23_Test(){
	  Machine m = new Machine();
	  List<String> num23 = new ArrayList<String>();
	  num23.add("JMP 2");
	  num23.add("RET R0");
	  m.execute(num23);
  }
  
  @Test
  public void num24_Test(){
	  Machine m = new Machine();
	  List<String> num24 = new ArrayList<String>();
	  num24.add("JMP 1");
	  num24.add("RET R0");
	  final int actual = 0;
	  assertEquals(m.execute(num24), actual);
  }
  
  @Test
  public void num25_Test(){
	  Machine m = new Machine();
	  List<String> num25 = new ArrayList<String>();
	  num25.add("MOV R0 -65535");
	  num25.add("RET R0");
	  final int actual = -65535;
	  assertEquals(m.execute(num25), actual);
  }
  
  @Test
  public void num26_Test(){
	  Machine m = new Machine();
	  List<String> num26 = new ArrayList<String>();
	  num26.add("MOV R0 65535");
	  num26.add("RET R0");
	  final int actual = 65535;
	  assertEquals(m.execute(num26), actual);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void num27_Test(){
	  Machine m = new Machine();
	  List<String> num27 = new ArrayList<String>();
	  num27.add("MOV R0 -65536");
	  num27.add("RET R0");
	  m.execute(num27);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void num28_Test(){
	  Machine m = new Machine();
	  List<String> num28 = new ArrayList<String>();
	  num28.add("MOV R0 65536");
	  num28.add("RET R0");
	  m.execute(num28);
  }
  
  @Test(expected = InvalidInstructionException.class)
  public void num29_Test(){
	  Machine m = new Machine();
	  List<String> num29 = new ArrayList<String>();
	  num29.add("MOV R32 2");
	  num29.add("RET R0");
	  m.execute(num29);
  }
  
  @Test
  public void num30_Test(){
	  Machine m = new Machine();
	  List<String> num30 = new ArrayList<String>();
	  num30.add("MOV R31 2");
	  num30.add("RET R31");
	  final int actual = 2;
	  assertEquals(m.execute(num30), actual);
  }
  
  @Test
  public void num31_Test(){
	  Machine m = new Machine();
	  List<String> num31 = new ArrayList<String>();
	  num31.add("STR R1 0 R0");
	  num31.add("RET R1");
	  final int actual = 0;
	  assertEquals(m.execute(num31), actual);
  }
  
  @Test
  public void num32_Test(){
	  Machine m = new Machine();
	  List<String> num32 = new ArrayList<String>();
	  num32.add("STR R0 65535 R1");
	  num32.add("RET R0");
	  final int actual = 0;
	  assertEquals(m.execute(num32), actual);
  }
  
  @Test
  public void num33_Test(){
	  Machine m = new Machine();
	  List<String> num33 = new ArrayList<String>();
	  num33.add("STR R0 -65535 R1");
	  num33.add("RET R0");
	  final int actual = 0;
	  assertEquals(m.execute(num33), actual);
  }
  
}
